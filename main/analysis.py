import os
import datetime
import pickle

import numpy as np
from PIL import Image

from main import utilities

test_images = {
    "000032451.tif": {"load_point": "00", "exp_number": "13"},
    "000034263.tif": {"load_point": "16", "exp_number": "13"},
    "000041488.tif": {"load_point": "00", "exp_number": "20"},
    "000042136.tif": {"load_point": "36", "exp_number": "20"},
    # "000046247.tif": {"load_point": "00", "exp_number": "23"},
    # "000047267.tif": {"load_point": "29", "exp_number": "23"},
    "000053585.tif": {"load_point": "00", "exp_number": "27"},
    "000054007.tif": {"load_point": "05", "exp_number": "27"},
    # "000054883.tif": {"load_point": "00", "exp_number": "28"},
    # "000057383.tif": {"load_point": "09", "exp_number": "28"},
    # "FFR04LP00.tif": {"load_point": "00", "exp_number": "FF"},
    # "FFR04LP17.tif": {"load_point": "17", "exp_number": "FF"},
    # "NFR04LP00.tif": {"load_point": "00", "exp_number": "NF"},
    # "NFR04LP17.tif": {"load_point": "17", "exp_number": "NF"},
}


class Analysis:
    def __init__(self, analysis_name, work_dir, temp_dir, image_dir):
        self.name = analysis_name
        self.info = None
        self.work_dir = work_dir
        self.temp_dir = temp_dir
        self.image_dir = image_dir
        self.source_image_name = None
        self.target_image_name = None
        self.data = None
        self.shape = (0, 0)
        self.column_dict = dict(
            x0=0,
            y0=1,
            u=2,
            v=3,
            ux=4,
            uy=5,
            vx=6,
            vy=7,
            w=8,
            Ex=9,
            Ey=10,
            Exy=11,
            Wxy=12,
            Emises=13,
            Emaxsh=14,
            func=15,
            iter=16,
            time=17,
            quality=18,
            degrade=19,
            outlier=20,
            failure=21,
        )
        self.operations = []
        return

    def __repr__(self):
        string = "Operation history:" + "\n"

        for operation, parameters in self.operations:
            string += "\t" + str(operation) + ": " + str(parameters) + "\n"

        return string

    def set_images(self, source_image_name, target_image_name):
        self._get_info(source_image_name, target_image_name)
        self.source_image_name = source_image_name.strip(".tif")
        self.target_image_name = target_image_name.strip(".tif")
        source_image_dir = os.path.join(self.image_dir, source_image_name)
        target_image_dir = os.path.join(self.image_dir, target_image_name)
        source_image = utilities.read_image(source_image_dir)
        target_image = utilities.read_image(target_image_dir)
        self.save_images(source_image, target_image)
        return source_image, target_image

    def set_data(self, spacing, padding, column_dict=None):
        source_image, __ = self.load_images()
        data_grid = utilities.make_centered_grid(source_image, spacing, padding)

        if column_dict is not None:
            self.column_dict = column_dict

        self.data = np.zeros((len(data_grid), len(self.column_dict)))
        self.data[:, 0:2] = np.array(data_grid)
        self._get_shape()
        return

    def set_operations(self, operations=[]):
        self.operations.append(operations)
        return

    def set_strains(self, mode="field"):

        if mode == "local":
            ux, uy, vx, vy = self.data.T[4:8]
        elif mode == "field":
            u, v = self.data.T[2:4]
            uy, ux = np.gradient(u.reshape(self.shape))
            vy, vx = np.gradient(v.reshape(self.shape))
            ux = ux.ravel() / 10.0
            uy = uy.ravel() / 10.0
            vx = vx.ravel() / 10.0
            vy = vy.ravel() / 10.0
        else:
            raise ValueError

        ex, ey, exy, wxy = ux, vy, 0.5 * (uy + vx), 0.5 * (uy - vx)

        nu = 0.5
        E1 = ((ex + ey) / 2) + np.sqrt(((ex - ey) / 2) ** 2 + exy ** 2)
        E2 = ((ex + ey) / 2) - np.sqrt(((ex - ey) / 2) ** 2 + exy ** 2)
        E3 = -(nu / (1 - nu)) * (ex + ey)
        emises = 1.0 / (np.sqrt(2.0) * (1 + nu))
        emises = emises * np.sqrt((E1 - E2) ** 2 + (E3 - E2) ** 2 + (E1 - E3) ** 2)

        emaxsh = np.sqrt((ey - ex) ** 2 + exy ** 2)

        self.data.T[9:15] = ex, ey, exy, wxy, emises, emaxsh
        return

    def _get_info(self, image_name1, image_name2):
        en1 = test_images[image_name1]["exp_number"]
        en2 = test_images[image_name2]["exp_number"]

        if en1 != en2:
            raise ValueError

        lp1 = test_images[image_name1]["load_point"]
        lp2 = test_images[image_name2]["load_point"]

        self.info = "%s%s%s" % (en1, lp1, lp2)
        return

    def _get_shape(self):
        shape = (len(np.unique(self.data[:, 1])), len(np.unique(self.data[:, 0])))
        self.shape = shape
        return

    def _get_mask(self):
        fail = self.data[:, 21].copy()
        fail[fail.astype(np.bool)] = np.nan
        fail = np.reshape(fail, self.shape)
        fail_y, fail_x = np.gradient(fail)
        fail = fail + fail_x + fail_y
        mask = fail.ravel()
        return mask

    def save_images(self, source_image, target_image):
        source_temp_dir = os.path.join(self.temp_dir, self.source_image_name)
        target_temp_dir = os.path.join(self.temp_dir, self.target_image_name)
        np.save(source_temp_dir, source_image)
        np.save(target_temp_dir, target_image)
        return

    def load_images(self):
        source_image_name = self.source_image_name + ".npy"
        target_image_name = self.target_image_name + ".npy"
        source_temp_dir = os.path.join(self.temp_dir, source_image_name)
        target_temp_dir = os.path.join(self.temp_dir, target_image_name)
        source_image = np.load(source_temp_dir)
        target_image = np.load(target_temp_dir)
        return source_image, target_image

    def pick_variable(self, key, grid=False, mask=False):
        col = self.column_dict[key]
        variable = self.data[:, col].copy()

        if mask:
            variable = variable + self._get_mask()

        if grid:
            variable = np.reshape(variable, self.shape)

        return variable

    def pickle_self(self):
        date_time = datetime.datetime.now()
        version = "%02d%02d_%02d%02d" % (
            date_time.month,
            date_time.day,
            date_time.hour,
            date_time.minute,
        )
        pickle_name = self.name + "_" + version
        pickle_file = os.path.join(self.work_dir, pickle_name)

        with open(pickle_file, "wb") as dump:
            pickle.dump(self, dump)

        return
