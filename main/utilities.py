import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from scipy import stats
from skimage import transform
from sklearn import cluster, preprocessing

import cv2


def read_image(name):
    """
    Reads image from disk as grayscale. Converts image dtype from uint8
    to float64.
    PARAMETERS:
        name:
            Name of image as it appears on the disk.
    RETURNS:
        Image as numpy array.
    """

    with Image.open(name) as image_file:
        image = np.asarray(image_file, dtype=np.float)

    return image


def save_image(array, name):
    """
    Saves arrays to disk as grayscale. Rounds gray-level values to
    nearest integers.
    PARAMETERS:
        array:
            Array to be saved as image.
        name:
            Name of image as it appears on the disk.
    RETURNS:
        Image as numpy array.
    """

    normalized_array = (array - array.min()) / (array.max() - array.min())
    normalized_array = normalized_array * 255
    image = Image.fromarray(normalized_array.round().astype(np.uint8))
    image.save(name)
    return


def vector2matrix(vector):
    """
    Transforms a (6,) deformation vector into a (2, 3) affine transform
    matrix.
    PARAMETERS:
        vector:
            The deformation vector to be transformed into an affine
            transform.
    RETURNS:
        Affine transform matrix.
    """

    identity_matrix = np.eye(2, 3)
    matrix = np.reshape(vector, (2, 3), order="F")
    matrix = np.roll(matrix, -1, axis=1) + identity_matrix
    return matrix


def matrix2vector(matrix):
    """
    Transforms a (2, 3) affine transform matrix into a (6,) deformation
    vector.
    PARAMETERS:
        matrix:
            The affine transform matrix to be transformed into a
            deformation vector.
    RETURNS:
        Deformation vector.
    """

    identity_matrix = np.eye(2, 3)
    vector = np.roll(matrix - identity_matrix, 1, axis=1)
    vector = np.reshape(vector, 6, order="F")
    return vector


def translate_affine(matrix, dx, dy):
    """
    Translates center of the applied affine transform by a given amount.
    This is particularly useful when computed deformation is used to
    transform an image; because deformation is calculated at center but
    affine transform is calculated at top-left.
    PARAMETERS:
        matrix:
            The affine transformation matrix.
        dx:
            Translation in the x-coordinate.
        dy:
            Translation in the y-coordinate.
    RETURNS:
        Translated affine matrix.
    """

    translated_affine = matrix.copy()
    translated_affine[:, 2] = np.dot(matrix, [dx, dy, 1]) - [dx, dy]
    return translated_affine


def make_image_grid(
    image,
    row0,
    col0,
    drow,
    dcol,
    nrow,
    ncol,
    grid_type="rowwise",
    show=False,
    reverse=False,
):
    """
    Forms a list that consists of GridPoint instances.
    PARAMETERS:
        row0, col0:
            First grid point location (top-left).
        drow, dcol:
            Spacing between each row and column.
        nrow, ncol:
            Number of rows and columns.
        grid_type:
            Type of grid.
                "rowwise": row by row, from left to right.
                "colwise": column by column, returns to top for each.
                "snakeLR": first row left right, second row right left, etc.
                "snakeUD": first column down, second column up, etc.
        show_grid:
            If True, plots the grid on top of image.
        reverse:
            If True, reverses  order of grid points.
    RETURNS:
        A list of grid points.
    """

    grid_list = []
    cols = np.arange(col0, col0 + dcol * ncol, dcol, dtype=np.int)
    rows = np.arange(row0, row0 + drow * nrow, drow, dtype=np.int)
    cols, rows = np.meshgrid(cols, rows)

    if grid_type == "rowwise":
        grid_list = np.vstack((cols.flatten(), rows.flatten()))
    elif grid_type == "colwise":
        grid_list = np.vstack((cols.T.flatten(), rows.T.flatten()))
    elif grid_type == "snakeLR":
        to_flip = np.arange(int(nrow / 2)) * 2 + 1
        temp = cols[to_flip, :].copy()
        cols[to_flip, :] = np.fliplr(temp)
        grid_list = np.vstack((cols.flatten(), rows.flatten()))
    elif grid_type == "snakeUD":
        to_flip = np.arange(int(ncol / 2)) * 2 + 1
        temp = rows[:, to_flip].copy()
        rows[:, to_flip] = np.flipud(temp)
        grid_list = np.vstack((cols.T.flatten(), rows.T.flatten()))
    else:
        raise ValueError("Unsupported grid type")

    if show:
        plt.figure()
        plt.imshow(image, cmap="gray")
        plt.plot(grid_list[0, :], grid_list[1, :], "o-")
        plt.show()

    if reverse:
        grid_list.reverse()

    gridpoint_list = grid_list.T.tolist()
    return gridpoint_list


def make_centered_grid(image, spacing, pad, roi=None, show=False):
    rsize, csize = image.shape

    if roi is None:
        roi = (0, 0, csize, rsize)

    xi, yi, xf, yf = roi

    ncols = ((xf - xi) - 2 * pad + spacing) // (spacing + 1)
    nrows = ((yf - yi) - 2 * pad + spacing) // (spacing + 1)

    cpad = ((xf - xi) - 2 * pad - ncols * (spacing + 1) + spacing) // 2
    rpad = ((yf - yi) - 2 * pad - nrows * (spacing + 1) + spacing) // 2

    rows, cols = np.mgrid[0:nrows, 0:ncols] * (spacing + 1) + pad
    cols = cols + xi + cpad
    rows = rows + yi + rpad
    grid_list = np.vstack((cols.flatten(), rows.flatten()))

    if show:
        plt.imshow(image)
        plt.plot(grid_list[0, :], grid_list[1, :], "o-")
        plt.show()

    return grid_list.T.tolist()


def make_subset_grid(size, center=None, spacing=0, meshed=False):
    window = 2 * size + 1
    num = (window + spacing) // (spacing + 1)
    pad = (window - num * (spacing + 1) + spacing) // 2

    dx = np.linspace(-size + pad, size - pad, num=num, dtype=np.int)
    dy = np.linspace(-size + pad, size - pad, num=num, dtype=np.int)

    if meshed:
        dx, dy = np.meshgrid(dx, dy)

    if center is None:
        x_grid = dx
        y_grid = dy
    else:
        x_grid = dx + center[0]
        y_grid = dy + center[1]

    return x_grid, y_grid


def get_subset(array, size, center):
    """
    Extracts a subset from an array.
    PARAMETERS:
        array:
            The array, from which the subset is to be extracted.
        size:
            Size of the subset, which is in the form of (2size+1)x(2size+1).
        center:
            Center coordinates of the subset.
    RETURNS
        Extracted subset array.
    """

    x0, y0 = np.round(center).astype(np.int)
    subset = array[(y0 - size) : (y0 + size + 1), (x0 - size) : (x0 + size + 1)]
    return subset


def warp_coordinates(x, y, vector):
    x_warped = x + vector[0] + vector[2] * x + vector[4] * y
    y_warped = y + vector[1] + vector[3] * y + vector[5] * x
    return x_warped, y_warped


def warp_array(array, matrix, direction="backward", method="fast"):
    """
    Applies an affine transformation to the given array. Sub-pixel coordinates
    are estimated using spline interpolation.
    PARAMETERS:
        array:
            The array to be warped.
        matrix:
            Inverse of the affine matrix, which transforms output array into
            the input array.
        direction:
            The direction of the transformation. If "forward", source array is
            deformed to match target array, if "backward", target array is
            deformed to match the source array. The default is "backward".
        method:
            Determines the image transformation method. If "fast", an opencv
            accelerated function with bicubic interpolation is used. If
            "accurate", a scikit-image function with biquintic interpolation is
            used. The default is "fast".
    RETURNS:
        Warped array.
    """

    if direction == "forward":
        matrix = np.linalg.inv(matrix)
    elif direction == "backward":
        flags = cv2.WARP_INVERSE_MAP
    else:
        raise ValueError("Unsupported parameter.")

    if method == "accurate":

        if matrix.shape == (2, 3):
            matrix = np.vstack((matrix, [0.0, 0.0, 1.0]))

        warped_array = transform.warp(array, matrix, order=5, mode="edge")
    elif method == "fast":
        flags = flags + cv2.INTER_CUBIC
        matrix = matrix.astype(np.float32)
        warped_array = cv2.warpAffine(array, matrix, array.T.shape, flags=flags)
    else:
        raise ValueError("Unsupported parameter.")

    return warped_array


def slice_array(array, x_divider=1, y_divider=1, show=False):
    """
    Slices an array into user defined pieces.
    PARAMETERS:
        array:
            The array to be sliced.
        x_divider, y_divider:
            Indices or sections that specify how the given array array will be
            divided. If a list, array is sliced at the coordinates given in
            each list. If an int, array is evenly divided into that many equal
            pieces (if it cannot be divided evenly, the remainder is added to
            the first slice so that no array slice is left out).
        show:
            If True, shows the slices in a single figure.
    RETURNS:
        A 2-D list of sliced array arrays.

    """
    sliced_array = np.array_split(array, y_divider, axis=0)
    rows = len(sliced_array)

    for i in range(rows):
        sliced_array[i] = np.array_split(sliced_array[i], x_divider, axis=1)
        cols = len(sliced_array[i])

    if show:
        plt.figure()

        for row, array_row in enumerate(sliced_array):
            for col, array_slice in enumerate(array_row):
                index = row * cols + col + 1
                plt.subplot(rows, cols, index)
                plt.imshow(array_slice, cmap="gray")

        plt.axis("off")
        plt.show()

    return sliced_array


def random_perturbation(vector, sigma=None):

    if sigma is None:
        sigma = 10 ** (np.around(np.log10(np.abs(vector))) - 1)

    p_perturbed = np.random.normal(vector, sigma)
    return p_perturbed
