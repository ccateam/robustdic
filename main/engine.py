import time

import numpy as np
from scipy import interpolate, ndimage, optimize
from skimage import feature, measure

import cv2
from main import utilities


def ssim_coefficient(p, f, spline, x0, y0, dx, dy):
    x = x0 + dx + p[0] + p[2] * dx + p[4] * dy
    y = y0 + dy + p[1] + p[3] * dx + p[5] * dy
    g = _evaluate(spline, x, y, dx=0, dy=0) + p[6]
    c = 1 - measure.compare_ssim(f, g, data_range=255)
    return c


def nmse_coefficient(p, f, spline, x0, y0, dx, dy):
    x = x0 + dx + p[0] + p[2] * dx + p[4] * dy
    y = y0 + dy + p[1] + p[3] * dx + p[5] * dy
    g = _evaluate(spline, x, y, dx=0, dy=0) + p[6]
    c = np.sum((f - g) ** 2) / np.sum(f ** 2)
    return c


def nmse_jacobian(p, f, spline, x0, y0, dx, dy):
    x = x0 + dx + p[0] + p[2] * dx + p[4] * dy
    y = y0 + dy + p[1] + p[3] * dx + p[5] * dy
    g = _evaluate(spline, x, y, dx=0, dy=0) + p[6]
    dg = np.ones((len(p), f.shape[0], f.shape[1]))
    dg[0] = _evaluate(spline, x, y, dx=0, dy=1)
    dg[1] = _evaluate(spline, x, y, dx=1, dy=0)
    dg[2] = dg[0] * dx
    dg[3] = dg[1] * dx
    dg[4] = dg[0] * dy
    dg[5] = dg[1] * dy
    j = -2 * np.sum((f - g) * dg, axis=(1, 2)) / np.sum(f ** 2)
    return j


def nmse_hessian(p, f, spline, x0, y0, dx, dy):
    x = x0 + dx + p[0] + p[2] * dx + p[4] * dy
    y = y0 + dy + p[1] + p[3] * dx + p[5] * dy
    dg = np.ones((len(p), f.shape[0], f.shape[1]))
    dg[0] = _evaluate(spline, x, y, dx=0, dy=1)
    dg[1] = _evaluate(spline, x, y, dx=1, dy=0)
    dg[2] = dg[0] * dx
    dg[3] = dg[1] * dx
    dg[4] = dg[0] * dy
    dg[5] = dg[1] * dy
    h = 2 * np.tensordot(dg, dg, axes=([1, 2], [1, 2])) / np.sum(f ** 2)
    return h


def _evaluate(spline, x, y, dx=0, dy=0, extrapolate=False):

    if not extrapolate:
        ty, tx = spline.get_knots()
        x_min, x_max = tx[0], tx[-1]
        y_min, y_max = ty[0], ty[-1]

        if not np.logical_and(
            np.logical_and(np.all(x <= x_max), np.all(x >= x_min)),
            np.logical_and(np.all(y <= y_max), np.all(y >= y_min)),
        ):
            raise ValueError("Coordinate out of bounds")

    subset = spline.ev(y, x, dx=dx, dy=dy)

    return subset


def coarse_search(source_subset, target_subset, x0, y0, p, method="brute", **kwargs):
    """
    Performs a coarse search between source and target subsets.
    PARAMETERS:
        source_subset:
            Source subset with a shape (2sub+1)x(2sub+1).
        target_subset:
            Target subset with a shape (2nrt+1)x(2nrt+1).
        x0, y0:
            Source subset center coordinates.
        p:
            Initial guess vector for deformation.
        method:
            Coarse search method. No matter the method is, the result is
            given as a vector. The default is "brute".
                "brute":    Finds the integer shift. Fastest among the
                            options.
                "subpixel": Finds the subpixel shift (10th of a pixel).
                            Slower than "brute" method.
    RETURNS
        Estimated deformation vector.
    """
    pad = (len(target_subset) - len(source_subset)) // 2

    if method == "brute":
        source_subset = source_subset.astype(np.float32)
        target_subset = target_subset.astype(np.float32)
        match = cv2.matchTemplate(source_subset, target_subset, cv2.TM_CCOEFF_NORMED)
        du, dv = np.array(cv2.minMaxLoc(match)[3]) - pad
    elif method == "subpixel":
        target_subset = target_subset[pad:-pad, pad:-pad]
        print(target_subset.shape)
        shift, _, _ = feature.register_translation(
            target_subset, source_subset, upsample_factor=10
        )
        dv, du = shift
    else:
        raise ValueError("Unsupported parameter.")

    result = p + np.array([du, dv, 0.0, 0.0, 0.0, 0.0])
    result = result.astype(np.float64)
    return result


def fine_search(
    source_subset,
    target_spline,
    x0,
    y0,
    dx,
    dy,
    p,
    tol=1e-05,
    maxiter=200,
    index="NMSE",
    method="trust-ncg",
    **kwargs
):
    """
    Performs a fine search between source and target subsets.
    PARAMETERS:
        source_subset:
            Source subset.
        target_spline:
            Spline object of target subset.
        x0, y0:
            Source subset center coordinates.
        dx, dy:
            Local mapping coordinates within a subset.
        p:
            Initial guess vector for deformation.
        tol:
            Tolerance for termination criterion. Default is 1e-05.
        maxiter:
            Maximum number of iterations allowed. Default is 200.
        index:
            Preferred index used as cost function. Default is "NMSE".
        method:
            Fine search method. One should refer to the documentation of
            scipy.optimize.minimize for all available methods. Default
            is "trust-ncg".
    RETURNS
        OptimizeResult object that represents the result.
    """
    args = (source_subset, target_spline, x0, y0, dx, dy)
    cost = CostFunction(index=index)
    result = optimize.minimize(
        cost.coefficient,
        p,
        args=args,
        method=method,
        tol=tol,
        jac=cost.jacobian,
        hess=cost.hessian,
        options={"maxiter": maxiter},
    )
    return result


def stochastic_search(
    source_subset,
    target_spline,
    x0,
    y0,
    dx,
    dy,
    bounds,
    tol=1e-05,
    maxiter=200,
    index="NMSE",
    **kwargs
):
    """
    Performs a stochastic search between source and target subsets.
    PARAMETERS:
        source_subset:
            Source subset.
        target_spline:
            Spline object of target subset.
        x0, y0:
            Source subset center coordinates.
        dx, dy:
            Local mapping coordinates within a subset.
        bounds:
            Bounds of each parameter for deformation.
        tol:
            Tolerance for termination criterion. Default is 1e-05.
        maxiter:
            Maximum number of iterations allowed. Default is 200.
        index:
            Preferred index used as cost function. Default is "NMSE".
    RETURNS
        OptimizeResult object that represents the result.
    """
    args = (source_subset, target_spline, x0, y0, dx, dy)
    cost = CostFunction(index=index)
    result = optimize.differential_evolution(
        cost.coefficient, bounds, args=args, tol=tol, maxiter=maxiter
    )
    return result


class CostFunction:
    """
    Class that combines different sets of functions to be used in
    optimization for the given cost function index.
    """

    def __init__(self, index="SSIM"):
        self.index = index
        if index == "SSIM":
            self.coefficient = ssim_coefficient
            self.jacobian = None
            self.hessian = None
        elif index == "NMSE":
            self.coefficient = nmse_coefficient
            self.jacobian = nmse_jacobian
            self.hessian = nmse_hessian
        return


class Search:
    def __init__(
        self, source, target, sub, nrt, nsb, order=5, method_list=[["fine", dict()]]
    ):
        self.source = source
        self.target = target
        self.order = order
        self.spline = self.interpolate_image()
        self.sub = sub
        self.nrt = nrt
        self.nsb = nsb
        self.method_list = method_list
        self._dy, self._dx = np.mgrid[-sub : sub + 1, -sub : sub + 1]
        return

    def __call__(self, point):
        start_time = time.time()
        x0, y0 = point[0:2].astype(np.int)
        p0 = point[2:9]
        w0 = p0[6]
        u0, v0 = np.round(p0[0:2]).astype(np.int)
        xt = x0 + u0
        yt = y0 + v0

        try:
            f = self.source[
                y0 - self.sub : y0 + self.sub + 1, x0 - self.sub : x0 + self.sub + 1
            ]
            g = self.target[
                yt - self.nrt : yt + self.nrt + 1, xt - self.nrt : xt + self.nrt + 1
            ]
            s = self.interpolate_subset((xt, yt))

            for method, kwargs in self.method_list:

                if method == "coarse":
                    result = coarse_search(f, g, x0, y0, p0[:6], **kwargs)
                    result = np.append(result, w0)
                    p0 = result.copy()
                elif method == "fine":
                    result = fine_search(f, s, x0, y0, self._dx, self._dy, p0, **kwargs)
                    p0 = result.x
                elif method == "stochastic":
                    w0 = 0.5
                    w1 = 0.5 * ((self.nsb - w0) / self.sub)
                    widths = np.array([w0, w0, w1, w1, w1, w1, w0])
                    bounds = [(i, j) for i, j in zip(p0 - widths, p0 + widths)]
                    bounds = tuple(bounds)
                    result = stochastic_search(
                        f, s, x0, y0, self._dx, self._dy, bounds, **kwargs
                    )
                    p0 = result.x

        except Exception:
            result = None

        if isinstance(result, np.ndarray):
            point[2:9] = result
            point[21] = False
        elif isinstance(result, optimize.OptimizeResult):
            point[2:9] = result.x
            point[15] = result.fun
            point[16] = result.nit
            point[21] = np.invert(result.success)
        else:
            # Setting initial guess back as result in case of failure
            point[2:9] = p0.copy()
            point[21] = True

        point[17] = time.time() - start_time
        return point

    def interpolate_image(self):
        """
        Interpolates given image fully, by using bivariate splines.
        RETURNS:
            Spline coefficients and interpolation order.
        """

        coefficients = ndimage.spline_filter(self.target, order=self.order)
        return coefficients

    def interpolate_subset(self, center):
        """
        Retrieves subset spline coefficients from given image spline
        coefficients.
        PARAMETERS:
            center:
                Subset center.
        RETURNS:
            A BivariateSpline object that consists of knots and spline
            coefficients of the subset.
        """
        center = np.around(center).astype(np.int)
        size = self.sub + self.nsb

        if self.order == 1:
            pad = 1
        elif self.order == 2 or self.order == 3:
            pad = 2
        elif self.order == 4 or self.order == 5:
            pad = 3
        else:
            raise ValueError("Unsupported interpolation order.")

        if self.order % 2 == 0:
            trim = 0.5
        else:
            trim = 0.0

        subset_tx, subset_ty = utilities.make_subset_grid(size - trim + pad, center)
        cx_coords, cy_coords = utilities.make_subset_grid(size, center, meshed=True)
        subset_c = self.spline[cy_coords, cx_coords].ravel()
        # Creating a spline object directly using BivariateSpline is not
        # recommended but it is faster to do so in our case.
        subset_spline = interpolate.BivariateSpline()
        subset_spline.degrees = (self.order, self.order)
        subset_spline.tck = (subset_ty, subset_tx, subset_c)
        return subset_spline
