import numpy as np
from skimage import measure

from main import utilities
from modules.basemodule import BaseProcessor


class MainProcessor(BaseProcessor):
    """
    Base class for processing applications.
    """

    def __call__(self, analysis_object):
        self._verify_object(analysis_object)
        source_image, __ = analysis_object.load_images()
        self._process(source_image, analysis_object.data)
        self._update_object(analysis_object)
        return

    def _process(self, image, data):
        raise NotImplementedError

    def _calculate_quality(self, subset):
        raise NotImplementedError


class SubsetEntropy(MainProcessor):
    def __init__(self, subset_size, **kwargs):
        self.parameters = dict(subset_size=subset_size)
        self.parameters.update(kwargs)
        return

    def _process(self, image, data):
        kwargs = self.parameters.copy()
        subset_size = kwargs.pop("subset_size")

        for point in data:
            x0, y0 = point[0:2]
            subset = utilities.get_subset(image, subset_size, (x0, y0))
            quality = self._calculate_quality(subset)
            point[18] = quality

        return

    def _calculate_quality(self, subset):
        subset = np.round(subset).astype(np.uint8)
        entropy = measure.shannon_entropy(subset)
        return entropy


class SubsetDegradation(MainProcessor):
    def __init__(self, normalize=False, **kwargs):
        self.parameters = dict(normalize=normalize)
        self.parameters.update(kwargs)
        return

    def _process(self, image, data):
        kwargs = self.parameters.copy()
        normalize = kwargs.pop("normalize", False)

        overall_intensity_shift = np.median(data[:, 8])

        for point in data:
            degrade = np.abs(point[8] - overall_intensity_shift)

            if normalize:
                degrade = degrade / 255

            point[19] = degrade

        return
