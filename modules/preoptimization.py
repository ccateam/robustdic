import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate, ndimage
from skimage import feature, measure

import cv2
from main import utilities
from modules.basemodule import BaseOptimizer


class PreOptimizer(BaseOptimizer):
    def __call__(self, analysis_object):
        self._verify_object(analysis_object)
        source_image, target_image = analysis_object.load_images()
        data = self._optimize(source_image, target_image, analysis_object.data)
        analysis_object.data = data
        analysis_object.set_strains()
        self._update_object(analysis_object)
        analysis_object.pickle_self()
        return

    def _optimize(self, source, target, data):
        raise NotImplementedError

    def _apply_deformation(self, source_coords, target_coords, data):
        kwargs = self.parameters.copy()
        nrt = kwargs.pop("nrt", 1)

        for point in data:
            x0, y0 = point[0:2]
            idx = np.logical_and(
                np.logical_and(
                    source_coords[:, 0] >= x0 - nrt, source_coords[:, 0] <= x0 + nrt
                ),
                np.logical_and(
                    source_coords[:, 1] >= y0 - nrt, source_coords[:, 1] <= y0 + nrt
                ),
            )
            sc = source_coords[idx] - [x0, y0]
            tc = target_coords[idx] - [x0, y0]
            p = self._estimate_deformation(np.array(sc), np.array(tc))
            point[2:8] = p

        return data

    def _apply_elevation(self, source_image, target_image, data):
        kwargs = self.parameters.copy()
        nrt = kwargs.pop("nrt", 1)

        approx_image = self._approximate_image(source_image, target_image, data)

        for point in data:
            x0, y0 = point[0:2]
            source_subset = utilities.get_subset(source_image, nrt, (x0, y0))
            approx_subset = utilities.get_subset(approx_image, nrt, (x0, y0))
            w = self._estimate_elevation(source_subset, approx_subset)
            point[8] = w

        return data

    def _estimate_deformation(self, source_coords, target_coords):
        affine, inliers = cv2.estimateAffine2D(source_coords, target_coords)

        if (np.sum(inliers) / len(inliers)) < 0.1:
            raise Warning("Less than a 10th of matches are used.")

        result = utilities.matrix2vector(affine)
        return result

    def _estimate_elevation(self, source_subset, target_subset):
        result = np.mean(source_subset - target_subset)
        return result

    def _approximate_image(self, source_image, target_image, data):
        rows, cols = source_image.shape

        x_grid = np.unique(np.array(data)[:, 0])
        y_grid = np.unique(np.array(data)[:, 1])
        u_guess = data[:, 2].reshape(len(y_grid), len(x_grid))
        v_guess = data[:, 3].reshape(len(y_grid), len(x_grid))

        u_splines = interpolate.RectBivariateSpline(y_grid, x_grid, u_guess)
        v_splines = interpolate.RectBivariateSpline(y_grid, x_grid, v_guess)

        Y, X = np.mgrid[0:rows, 0:cols]
        U = u_splines.ev(Y, X)
        V = v_splines.ev(Y, X)
        x = X + U
        y = Y + V
        approx_image = ndimage.map_coordinates(target_image, (y, x), mode="nearest")
        similarity = measure.compare_ssim(source_image, approx_image, data_range=255)
        print("Approximation is {:.2%} similar.".format(similarity))

        utilities.save_image(source_image - approx_image, "degrade.tif")

        return approx_image


class FeatureMatch(PreOptimizer):
    def __init__(self, nrt, roi=None, **kwargs):
        self.parameters = dict(nrt=nrt, roi=roi)
        self.parameters.update(kwargs)
        return

    def _optimize(self, source, target, data):
        source_coords, target_coords = self._find_matches(source, target)
        data = self._apply_deformation(source_coords, target_coords, data)
        data = self._apply_elevation(source, target, data)
        return data

    def _find_matches(self, source, target):
        source_kpts, source_desc = self._detect_features(source)
        target_kpts, target_desc = self._detect_features(target)

        source_coords, target_coords = self._match_features(
            source_kpts, source_desc, target_kpts, target_desc
        )

        return source_coords, target_coords

    def _detect_features(self, image):
        """
        Detects ORB features of given image.
        PARAMETERS:
            image:
                Image to extract the descriptors from.
        RETURNS:
            Keypoints and descriptors of the image.
        """
        orb = cv2.ORB.create(nfeatures=int(1e8))
        keypoints, descriptors = orb.detectAndCompute(
            image.round().astype(np.uint8), None
        )
        return keypoints, descriptors

    def _match_features(self, source_kpts, source_desc, target_kpts, target_desc):
        """
        Matches features of two images.
        PARAMETERS:
            source_kpts, source desc:
                Keypoints and descriptors of the source image.
            target_kpts, target decs:
                Keypoints and descriptors of the target image.
        RETURNS
            Coordinates of matched features.
        """
        bf_matcher = cv2.BFMatcher.create(cv2.NORM_HAMMING, crossCheck=True)
        matches = bf_matcher.match(source_desc, target_desc)
        matches = sorted(matches, key=lambda x: x.distance)

        source_coords = [source_kpts[match.queryIdx].pt for match in matches]
        target_coords = [target_kpts[match.trainIdx].pt for match in matches]

        source_coords = np.array(source_coords)
        target_coords = np.array(target_coords)

        return source_coords, target_coords


class CascadedSearch(PreOptimizer):
    def __init__(self, nrt, roi=None, **kwargs):
        self.parameters = dict(nrt=nrt, roi=roi)
        self.parameters.update(kwargs)
        return

    def _optimize(self, source, target, data):
        source_coords, target_coords = self._find_matches(source, target)
        data = self._apply_deformation(source_coords, target_coords, data)
        data = self._apply_elevation(source, target, data)
        return data

    def _find_matches(self, source, target):
        kwargs = self.parameters.copy()
        min_size = kwargs.pop("nrt", 1)
        roi = kwargs.pop("roi", None)

        rows, cols = source.shape

        if roi is None:
            roi = (0, 0, cols, rows)

        xi, yi, xf, yf = roi

        [shift_y, shift_x], _, _ = feature.register_translation(
            target[slice(yi, yf), slice(xi, xf)], source[slice(yi, yf), slice(xi, xf)]
        )

        max_size = (min(xf - xi, yf - yi) - 1) // 2
        nit = int(np.ceil(np.log2(max_size // min_size)))

        X = np.atleast_1d((xi + xf - 1) * 0.5)
        Y = np.atleast_1d((yi + yf - 1) * 0.5)
        u_splines = interpolate.NearestNDInterpolator((Y, X), np.atleast_2d(shift_x))
        v_splines = interpolate.NearestNDInterpolator((Y, X), np.atleast_2d(shift_y))

        for i in range(nit):
            size = int(max_size // 2 ** (i + 1))
            print(size)
            dy, dx = np.mgrid[-size : size + 1, -size : size + 1]
            grid = utilities.make_centered_grid(source, spacing=size, pad=size, roi=roi)

            u_list = u_splines(np.fliplr(grid)).astype(np.int)
            v_list = v_splines(np.fliplr(grid)).astype(np.int)

            for j, (center, u, v) in enumerate(zip(grid, u_list, v_list)):
                x0, y0 = center
                x_f = np.clip(dx + x0, xi, xf - 1)
                y_f = np.clip(dy + y0, yi, yf - 1)
                x_g = np.clip(x_f + u, xi, xf - 1)
                y_g = np.clip(y_f + v, yi, yf - 1)

                f = source[y_f, x_f]
                g = target[y_g, x_g]

                if i < nit - 1:
                    [dv, du], _, _ = feature.register_translation(g, f)
                else:
                    [dv, du], _, _ = feature.register_translation(
                        g, f, upsample_factor=100
                    )

                u_list[j] = u + du
                v_list[j] = v + dv

            if i < nit - 1:
                X = np.unique(np.array(grid)[:, 0])
                Y = np.unique(np.array(grid)[:, 1])
                U = np.array(u_list).reshape(len(Y), len(X))
                V = np.array(v_list).reshape(len(Y), len(X))

                # Data are smoothed to remove possible errors:
                window_size = 2 * i + 1
                U = ndimage.median_filter(U, size=window_size)
                V = ndimage.median_filter(V, size=window_size)

                u_splines = interpolate.RegularGridInterpolator(
                    (Y, X), U, "nearest", False, None
                )
                v_splines = interpolate.RegularGridInterpolator(
                    (Y, X), V, "nearest", False, None
                )
            else:
                source_coords = np.array(grid) * 1.0
                target_coords = source_coords.copy()
                target_coords[:, 0] = target_coords[:, 0] + np.array(u_list)
                target_coords[:, 1] = target_coords[:, 1] + np.array(v_list)

        return source_coords, target_coords
