import numpy as np
from scipy import ndimage, signal
from skimage import filters, restoration

from modules.basemodule import BaseProcessor


class PreProcessor(BaseProcessor):
    """
    Base class for pre-processing applications.
    """

    def __call__(self, analysis_object):
        self._verify_object(analysis_object)
        source_image, target_image = analysis_object.load_images()
        source_image = self._process(source_image)
        target_image = self._process(target_image)
        analysis_object.save_images(source_image, target_image)
        self._update_object(analysis_object)
        return

    def _process(self, image):
        raise NotImplementedError


class DenoiseImages(PreProcessor):
    """
    Applies selected denoising filter to given images.
    """

    def __init__(self, size=3, mode="gaussian", **kwargs):
        self.parameters = dict(size=size, mode=mode)
        self.parameters.update(kwargs)
        return

    def _process(self, image):
        kwargs = self.parameters.copy()
        mode = kwargs.pop("mode")
        size = kwargs.pop("size")

        if mode == "gaussian":
            filtered_image = self._gaussian_filter(image, size, **kwargs)
        elif mode == "median":
            filtered_image = self._median_filter(image, size, **kwargs)
        elif mode == "savgol":
            filtered_image = self._savgol_filter(image, size, **kwargs)
        else:
            raise ValueError("Not a valid filtering mode.")

        return filtered_image

    def _gaussian_filter(self, image, size, **kwargs):
        sigma = 0.3 * ((size - 1) * 0.5 - 1) + 0.8
        trunc = kwargs.pop("truncate", ((size - 1) * 0.5) / sigma)
        filtered_image = ndimage.gaussian_filter(
            image, sigma=sigma, truncate=trunc, **kwargs
        )
        return filtered_image

    def _median_filter(self, image, size, **kwargs):
        filtered_image = ndimage.median_filter(image, size=size, **kwargs)
        return filtered_image

    def _savgol_filter(self, image, size, **kwargs):
        polyorder = kwargs.pop("polyorder", size - 2)
        kernel = signal.savgol_coeffs(size, polyorder=polyorder, **kwargs)
        filtered_image = signal.sepfir2d(image, kernel, kernel)
        return filtered_image


class ResizeImages(PreProcessor):
    """
    Resizes an image using spline interpolation.
    """

    def _process(self, image):
        kwargs = self.parameters.copy()
        factor = kwargs.pop("factor", 1.0)
        order = kwargs.pop("order", 5)
        resized_image = ndimage.zoom(image, zoom=factor, order=order, **kwargs)
        return resized_image
