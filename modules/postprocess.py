import numpy as np
from scipy import interpolate, stats
from sklearn import cluster, preprocessing

from main import utilities
from modules.basemodule import BaseProcessor


class PostProcessor(BaseProcessor):
    """
    Base class for post-process applications.
    """

    def __call__(self, analysis_object):
        self._verify_object(analysis_object)
        data = self._process(analysis_object.data)
        analysis_object.data = data
        self._update_object(analysis_object)
        return

    def _process(self, data):
        raise NotImplementedError


class ClusterData(PostProcessor):
    def __init__(self, n_clusters=1, normalize=True, sigma=4, **kwargs):
        self.parameters = dict(n_clusters=n_clusters, normalize=normalize, sigma=sigma)
        self.parameters.update(kwargs)
        return

    def _process(self, data):
        # Marking points that are not successful
        condition = data[:, 21] == True
        valid_indexes = np.flatnonzero(~condition)

        vectors = np.array([point[4:8] for point in data[valid_indexes]])
        outliers = self._find_outliers(vectors)

        outlier_indexes = valid_indexes[outliers]
        data[outlier_indexes, 20] = True

        return data

    def _find_outliers(self, data):
        """
        Clusters data using KMeans and finds outliers that are distant
        from cluster centers.
        PARAMETERS:
            data:
                The data from which outliers are found. Must be in the
                shape of (sample size)x(number of features).
            clusters:
                Number of clusters to divide the data into. The default
                is 1.
            sigma:
                Threshold that determines the accepted range. The
                default is 4.
        RETURNS:
            Row indexes of outliers.
        """
        kwargs = self.parameters.copy()
        n_clusters = kwargs.pop("n_clusters")
        normalize = kwargs.pop("normalize")
        sigma = kwargs.pop("sigma")

        if normalize:
            scaled_data = preprocessing.scale(data)
        else:
            scaled_data = data

        kmeans = cluster.KMeans(n_clusters, random_state=0, **kwargs)
        kmeans.fit(scaled_data)
        labels = kmeans.labels_
        cluster_centers = kmeans.cluster_centers_[labels]
        norms = np.linalg.norm(scaled_data - cluster_centers, axis=1)
        sigmaclip_result = stats.sigmaclip(norms, low=sigma, high=sigma)
        outliers = np.isin(norms, sigmaclip_result.clipped, invert=True)
        return outliers


class CompareData(PostProcessor):
    def __init__(self, sigma=4, **kwargs):
        self.parameters = dict(sigma=sigma)
        self.parameters.update(kwargs)
        return

    def _process(self, data):
        # Marking points that are not successful
        condition = data[:, 21] == True
        valid_indexes = np.flatnonzero(~condition)

        vectors = np.array([point[4:8] for point in data[valid_indexes]])
        outliers = self._find_outliers(vectors)

        outlier_indexes = valid_indexes[outliers]
        data[outlier_indexes, 20] = True

        return data

    def _find_outliers(self, data):
        """
        Clusters data using KMeans and finds outliers that are distant
        from cluster centers.
        PARAMETERS:
            data:
                The data from which outliers are found. Must be in the
                shape of (sample size)x(number of features).
            clusters:
                Number of clusters to divide the data into. The default
                is 1.
            sigma:
                Threshold that determines the accepted range. The
                default is 4.
        RETURNS:
            Row indexes of outliers.
        """
        kwargs = self.parameters.copy()
        sigma = kwargs.pop("sigma", 4)

        field_strains = data[9:13]
        local_strains = np.array(self._calculate_strains(data)).T
        norms = np.linalg.norm(field_strains - local_strains, axis=1)
        sigmaclip_result = stats.sigmaclip(norms, low=sigma, high=sigma)
        outliers = np.isin(norms, sigmaclip_result.clipped, invert=True)
        return outliers

    def _calculate_strains(self, data):
        ux, uy, vx, vy = data.T[4:8]
        ex, ey, exy, wxy = ux, vy, 0.5 * (uy + vx), 0.5 * (uy - vx)
        return ex, ey, exy, wxy


class FillData(PostProcessor):
    def __init__(self, which="both", **kwargs):
        self.parameters = dict(which=which)
        self.parameters.update(kwargs)
        return

    def _process(self, data):
        kwargs = self.parameters
        which = kwargs.pop("which", "both")

        if which == "failure":
            # Marking points that are not successful
            condition = data[:, 21] == True
        elif which == "outlier":
            # Marking points that are outliers
            condition = data[:, 20] == True
        elif which == "both":
            # Marking points that are either not successful or outliers
            condition = np.logical_or(data[:, 21] == True, data[:, 20] == True)
        else:
            raise ValueError

        valid_indexes = np.flatnonzero(~condition)
        invalid_indexes = np.flatnonzero(condition)

        valid_coords = data[valid_indexes, 0:2]
        invalid_coords = data[invalid_indexes, 0:2]

        for j in range(2, 9):
            valid_values = data[valid_indexes, j]
            interpolated_values = interpolate.griddata(
                valid_coords,
                valid_values,
                invalid_coords,
                method="cubic",
                fill_value=np.nanmedian(valid_values),
            )
            data[invalid_indexes, j] = interpolated_values

        return data
