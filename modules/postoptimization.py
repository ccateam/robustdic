import multiprocessing as mp

import numpy as np

from main import analysis, engine
from modules.basemodule import BaseOptimizer


class PostOptimizer(BaseOptimizer):
    def __call__(self, analysis_object):
        self._verify_object(analysis_object)
        source_image, target_image = analysis_object.load_images()
        data = self._optimize(source_image, target_image, analysis_object.data)
        analysis_object.data = np.array(data)
        analysis_object.set_strains()
        self._update_object(analysis_object)
        analysis_object.pickle_self()
        return

    def _optimize(self, source, target, data):
        raise NotImplementedError


class FillSearch(PostOptimizer):
    def __init__(self, sub, nrt, nsb, which="both", **kwargs):
        self.parameters = dict(sub=sub, nrt=nrt, nsb=nsb, which=which)
        self.parameters.update(kwargs)
        return

    def _optimize(self, source, target, data):
        kwargs = self.parameters.copy()
        which = kwargs.pop("which", "both")

        if which == "failure":
            # Marking points that are not successful
            condition = data[:, 21] == True
        elif which == "outlier":
            # Marking points that are outliers
            condition = data[:, 20] == True
        elif which == "both":
            # Marking points that are either not successful or outliers
            condition = np.logical_or(data[:, 21] == True, data[:, 20] == True)
        else:
            raise ValueError

        invalid_indexes = np.flatnonzero(condition)

        invalid_data = data[invalid_indexes]

        solver = engine.Search(source, target, **kwargs)
        processes = mp.cpu_count() - 1
        chunksize = max(len(invalid_data) // processes, 1)
        pool = mp.Pool(processes)
        filled_data = pool.map(solver, invalid_data, chunksize)
        pool.close()
        pool.join()
        pool.terminate()

        data[invalid_indexes] = filled_data
        data[invalid_indexes, 20] = False  # outliers marked clean again
        return data
