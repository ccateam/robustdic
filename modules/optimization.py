import multiprocessing as mp

import numpy as np

from main import analysis, engine
from modules.basemodule import BaseOptimizer


class Optimizer(BaseOptimizer):
    def __call__(self, analysis_object):
        self._verify_object(analysis_object)
        source_image, target_image = analysis_object.load_images()
        data = self._optimize(source_image, target_image, analysis_object.data)
        analysis_object.data = np.array(data)
        analysis_object.set_strains()
        self._update_object(analysis_object)
        analysis_object.pickle_self()
        return

    def _optimize(self, source, target, data):
        raise NotImplementedError


class GridSearch(Optimizer):
    def __init__(self, sub, nrt, nsb, **kwargs):
        self.parameters = dict(sub=sub, nrt=nrt, nsb=nsb)
        self.parameters.update(kwargs)
        return

    def _optimize(self, source, target, data):
        solver = engine.Search(source, target, **self.parameters)
        processes = mp.cpu_count() - 1
        chunksize = max(len(data) // processes, 1)
        pool = mp.Pool(processes)
        result = pool.map(solver, data, chunksize)
        pool.close()
        pool.join()
        pool.terminate()
        return result
