from main import analysis


class BaseModule(object):
    """
    Base class for all Robust DIC modules.
    """

    def __init__(self, **kwargs):
        self.parameters = dict()
        self.parameters.update(kwargs)
        return

    def __repr__(self,):
        string = "DIC module name:" + "\n"
        string += "\t" + self.__class__.__name__ + "\n"
        string += "Module parameters:" + "\n"

        for key in self.parameters:
            string += "\t" + str(key) + ": " + str(self.parameters[key]) + "\n"

        return string

    def __call__(self, analysis_object):
        raise NotImplementedError

    def _verify_object(self, object):

        if not isinstance(object, analysis.Analysis):
            raise ValueError

        return

    def _update_object(self, object):
        object.set_operations([self.__class__.__name__, self.parameters])
        return


class BaseProcessor(BaseModule):
    """
    Base class for processing applications.
    """

    def _process(self, stuff):
        raise NotImplementedError


class BaseOptimizer(BaseModule):
    """
    Base class for optimization applications.
    """

    def _optimize(self, stuff):
        raise NotImplementedError
