import pickle

import dicmadv.nplot as nplot
import dicmadv.npostProcess as npost

import matplotlib.pyplot as plt

mydict = {
    "X": 0,
    "Y": 1,
    "u": 2,
    "v": 3,
    "dudx": 4,
    "dudy": 5,
    "dvdx": 6,
    "dvdy": 7,
    "iter": 8,
    "corrcoeff": 9,
    "status": 10,
}

rd1 = pickle.load(open("C://Users//Necdet/robustdic/an1nmse.p", "r"))
rd2 = pickle.load(open("C://Users//Necdet/robustdic/an2nmse.p", "r"))

fd1 = npost.frameData(rawDatas=rd1, colDict=mydict, imageNum=0)
fd2 = npost.frameData(rawDatas=rd2, colDict=mydict, imageNum=0)

D1, keys = fd1.getStrainFrames()
D1, keys = fd2.getStrainFrames()

fig = plt.figure()
gsp = plt.GridSpec(nrows=2, ncols=2)

fig.text(0.5, 0.00, "Ey")
fig.text(0.5, 0.50, "Ex")
fig.text(0.00, 0.5, "# of occurances", rotation="vertical")

hist_pars = dict(percent=True, timesStd=3, bins=400, histtype="step")

ax00 = fig.add_subplot(gsp[0, 0])
procData, histKwds, histTxt = nplot.histProcessor(D1["Ex"].flatten(), **hist_pars)
ax00.hist(procData, **histKwds)
ax00.set_title(histTxt)

ax01 = fig.add_subplot(gsp[0, 1])
procData, histKwds, histTxt = nplot.histProcessor(D2["Ex"].flatten(), **hist_pars)
ax01.hist(procData, **histKwds)
ax01.set_title(histTxt)


hist_pars = dict(percent=True, timesStd=3, bins=400, histtype="step")

ax10 = fig.add_subplot(gsp[1, 0])
procData, histKwds, histTxt = nplot.histProcessor(D1["Ey"].flatten(), **hist_pars)
ax10.hist(procData, **histKwds)
ax10.set_title(histTxt)

ax11 = fig.add_subplot(gsp[1, 1])
procData, histKwds, histTxt = nplot.histProcessor(D2["Ey"].flatten(), **hist_pars)
ax11.hist(procData, **histKwds)
ax11.set_title(histTxt)

plt.show()
