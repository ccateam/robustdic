import os
import pickle

import dicmadv.nplot as nplot
import dicmadv.npostProcess as npost
import main.analysis as analysis

if __name__ == "__main__":
    homedir = os.path.expanduser("~")
    imgdir = os.path.join(homedir, "PyImages")
    image1 = os.path.join(imgdir, "reference.tif")
    image2 = os.path.join(imgdir, "synthetic.tif")
    frame = analysis.Frame(0, image1, image2, denoise=False)
    frame.global_search(show=False, x_divider=8, y_divider=8)
    frame.make_grid(
        row0=72,
        col0=71,
        drow=10,
        dcol=10,
        nrow=191,
        ncol=231,
        grid_type="rowwise",
        show=False,
        set_initial_coords=True,
    )
    minimizer = analysis.Minimizer(frame, sub=15, nrt=30, nsb=15)
    minimizer.solve()

    mydict = {
        "X": 0,
        "Y": 1,
        "u": 2,
        "v": 3,
        "dudx": 4,
        "dudy": 5,
        "dvdx": 6,
        "dvdy": 7,
        "iter": 8,
        "corrcoeff": 9,
        "status": 10,
    }

    raw_data = pickle.load(open("analysis.p", "r"))
    frame_data = npost.frameData(rawDatas=raw_data, colDict=mydict, imageNum=0)
    data, keys = frame_data.getStrainFrames()
