import matplotlib.pyplot as plt
import numpy as np
from skimage import data
from skimage.transform import PiecewiseAffineTransform, warp
from skimage.util import random_noise


def random_displacement(center, mean, std):
    """
    
    """
    x0, y0 = center

    if type(mean) is int:
        mean_x = mean
        mean_y = mean
    elif type(mean) is tuple or type(mean) is list:
        mean_x, mean_y = mean

    if type(std) is int:
        std_x = std
        std_y = std
    elif type(std) is tuple or type(std) is list:
        std_x, std_y = std

    x = x0 + mean_x + np.random.rand() * std_x
    y = y0 + mean_y + np.random.rand() * std_y

    return [x, y]


def add_deformation(image, grid, function, deformed_grid=None, **kwargs):
    """

    """

    if deformed_grid is None:
        deformed_grid = np.array([function(point, **kwargs) for point in grid])
    else:
        pass

    tform = PiecewiseAffineTransform()
    tform.estimate(grid, deformed_grid)
    deformed_image = warp(image, tform)

    return deformed_image, deformed_grid


def add_noise(image, mode="gaussian", mean=0.0, std=1.0, **kwargs):
    """

    """
    mean /= 255
    var = (std / 255) ** 2
    noisy_image = random_noise(image, mode=mode, mean=mean, var=var, **kwargs)
    noisy_image *= 255

    return noisy_image


def make_synthetic_image(image, grid, operation_list):
    """

    """

    synthetic_image = image
    final_grid = grid

    for operation, kwargs in operation_list:

        if operation == "add_noise":
            synthetic_image = add_noise(synthetic_image, **kwargs)
        elif operation == "add_deformation":
            synthetic_image, final_grid = add_deformation(
                synthetic_image, final_grid, **kwargs
            )
        else:
            raise ValueError("Not a valid operation")

        displacement_grid = final_grid - grid

    return synthetic_image, displacement_grid


if __name__ == "__main__":
    image = data.moon()

    rows, cols = image.shape[0], image.shape[1]

    src_cols = np.linspace(0, cols, 20)
    src_rows = np.linspace(0, rows, 10)
    src_rows, src_cols = np.meshgrid(src_rows, src_cols)
    src = np.dstack([src_cols.flat, src_rows.flat])[0]

    op_list = [
        ["add_noise", {"mean": 1, "std": 3}],
        ["add_deformation", {"function": random_displacement, "mean": 5, "std": 1}],
        ["add_deformation", {"function": random_displacement, "mean": 2, "std": 2}],
        ["add_deformation", {"function": random_displacement, "mean": 1, "std": 5}],
    ]

    new_image, def_list = make_synthetic_image(image, src, op_list)

    plt.figure()
    plt.subplot(121)
    plt.imshow(image, cmap="gray")
    plt.subplot(122)
    plt.imshow(new_image, cmap="gray")
    plt.show()
