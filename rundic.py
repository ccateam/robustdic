import os
import numpy as np
from main import analysis, utilities
from modules import (
    preprocess,
    preoptimization,
    process,
    optimization,
    postprocess,
    postoptimization,
)


if __name__ == "__main__":
    home_dir = os.path.expanduser("~")
    work_dir = os.path.join(home_dir, "dic-result")
    temp_dir = os.path.join(home_dir, "dic-result")
    image_dir = os.path.join(home_dir, "dic-images")

    source_file = "000041488.tif"
    target_file = "000042136.tif"
    name = "exp20_15"

    case = analysis.Analysis(name, work_dir, temp_dir, image_dir)
    case.set_images(source_file, target_file)
    case.set_data(9, 253)

    denoise = preprocess.DenoiseImages()
    cascade = preoptimization.CascadedSearch(15)
    entropy = process.SubsetEntropy(15)
    degrade = process.SubsetDegradation()
    gridopt = optimization.GridSearch(15, 30, 15, method_list=[["fine", {}]])
    cluster = postprocess.ClusterData()
    nanfill = postprocess.FillData(fill="both")
    fillopt = postoptimization.FillSearch(
        15, 30, 60, method_list=[["fine", {"method": "Powell"}]]
    )

    denoise(case)
    cascade(case)
    entropy(case)
    degrade(case)
