import os
import time
import numpy as np
import engine.interpolate as interpolate
import engine.search as search
from scipy import ndimage, optimize
from skimage import io, measure
import utilities.imageutils as imgutil
import utilities.arrayutils as arrutil


def coeff(p, f, spline, x0, y0, dx, dy):
    x = x0 + dx + p[0] + p[2] * dx + p[4] * dy
    y = y0 + dy + p[1] + p[3] * dx + p[5] * dy
    g = spline.ev(y, x, dx=0, dy=0) + p[6]
    c = 1 - measure.compare_ssim(
        f, g, data_range=255, win_size=1, use_sample_covariance=False
    )
    return c


def jacob(p, f, spline, x0, y0, dx, dy):
    x = x0 + dx + p[0] + p[2] * dx + p[4] * dy
    y = y0 + dy + p[1] + p[3] * dx + p[5] * dy
    g = spline.ev(y, x, dx=0, dy=0) + p[6]
    dg = np.ones((len(p), f.shape[0], f.shape[1]))
    dg[0] = spline.ev(y, x, dx=0, dy=1)
    dg[1] = spline.ev(y, x, dx=1, dy=0)
    dg[2] = dg[0] * dx
    dg[3] = dg[1] * dx
    dg[4] = dg[0] * dy
    dg[5] = dg[1] * dy
    __, ds = measure.compare_ssim(
        f, g, data_range=255, win_size=1, gradient=True, use_sample_covariance=False
    )
    j = -np.sum(ds * dg, axis=(1, 2))
    return j


###############################################################################
if __name__ == "__main__":
    homedir = os.path.expanduser("~")
    imgdir = os.path.join(homedir, "dic-images", "exp13")
    source_file = os.path.join(imgdir, "LP00", "000032451.tif")
    target_file = os.path.join(imgdir, "LP16", "000034263.tif")
    source = ndimage.gaussian_filter(io.imread(source_file) * 1.0, 0.8)
    target = ndimage.gaussian_filter(io.imread(target_file) * 1.0, 0.8)

    sub = 50
    nsb = 50

    [rows, cols] = np.shape(source)
    x0 = 1150
    y0 = 752

    p0 = np.array([2.0, 83.0, 0.0, 0.0, 0.0, 0.0, 30.0])
    u0 = int(np.around(p0[0]))
    v0 = int(np.around(p0[1]))

    f = arrutil.get_subset(source, sub, (x0, y0))
    target_spline = interpolate.interpolate_image(target, 5)
    spline = interpolate.interpolate_subset(
        target_spline, 5, sub + nsb, (x0 + u0, y0 + v0)
    )

    range_u = (p0[0] - 10, p0[0] + 10)
    range_v = (p0[1] - 10, p0[1] + 10)
    range_s = (-0.5, 0.5)
    range_w = (p0[6] - 10, p0[6] + 10)
    ranges = (range_u, range_v, range_s, range_s, range_s, range_s, range_w)

    dx, dy = arrutil.make_subset_grid(sub, meshed=True)

    args = (f, spline, x0, y0, dx, dy)

    startTime = time.time()
    # res = optimize.differential_evolution(coeff, ranges, args=args, tol=1e-5)
    endTime = time.time()
    print(res.message)
    print(res.x)
    print(endTime - startTime)
