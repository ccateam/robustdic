import os

import matplotlib.pyplot as plt
import numpy as np

from mpl_toolkits import axes_grid1
from scipy import interpolate, ndimage
from skimage import feature, filters, io, measure

from main import utilities

import cv2


def make_image_grid(image, spacing, pad, roi=None, show=False):
    rsize, csize = image.shape

    if roi is None:
        roi = (0, 0, csize, rsize)

    xi, yi, xf, yf = roi

    ncols = ((xf - xi) - 2 * pad + spacing) // (spacing + 1)
    nrows = ((yf - yi) - 2 * pad + spacing) // (spacing + 1)

    cpad = ((xf - xi) - 2 * pad - ncols * (spacing + 1) + spacing) // 2
    rpad = ((yf - yi) - 2 * pad - nrows * (spacing + 1) + spacing) // 2

    rows, cols = np.mgrid[0:nrows, 0:ncols] * (spacing + 1) + pad
    cols = cols + xi + cpad
    rows = rows + yi + rpad
    grid_list = np.vstack((cols.flatten(), rows.flatten()))

    if show:
        plt.imshow(image)
        plt.plot(grid_list[0, :], grid_list[1, :], "o-")
        plt.show()

    return grid_list.T.tolist()


def cascaded_search(image1, image2, min_size, roi=None):
    """
    Performs a cascaded search that refines the shift at each iteration.
    PARAMETERS
        image1, image2:
            Images to be searched on.
        nit:
            Number of iterations. Subset size is halved in each iteration.
    """
    rows, cols = image1.shape

    if roi is None:
        roi = (0, 0, cols, rows)

    xi, yi, xf, yf = roi

    [shift_y, shift_x], _, _ = feature.register_translation(
        image2[slice(yi, yf), slice(xi, xf)], image1[slice(yi, yf), slice(xi, xf)]
    )

    max_size = (min(xf - xi, yf - yi) - 1) // 2
    nit = int(np.floor(np.log2(max_size // min_size)))

    X = np.atleast_1d((xi + xf - 1) * 0.5)
    Y = np.atleast_1d((yi + yf - 1) * 0.5)
    u_splines = interpolate.NearestNDInterpolator((Y, X), np.atleast_2d(shift_x))
    v_splines = interpolate.NearestNDInterpolator((Y, X), np.atleast_2d(shift_y))

    for i in range(nit):
        size = int(max_size // 2 ** (i + 1))
        print(size)
        dy, dx = np.mgrid[-size : size + 1, -size : size + 1]
        grid = make_image_grid(image1, spacing=size, pad=size - 1, roi=roi)

        u_list = u_splines(np.fliplr(grid)).astype(np.int)
        v_list = v_splines(np.fliplr(grid)).astype(np.int)

        for j, (center, u, v) in enumerate(zip(grid, u_list, v_list)):
            x0, y0 = center
            x_f = np.clip(dx + x0, xi, xf - 1)
            y_f = np.clip(dy + y0, yi, yf - 1)
            x_g = np.clip(x_f + u, xi, xf - 1)
            y_g = np.clip(y_f + v, yi, yf - 1)

            f = image1[y_f, x_f]
            g = image2[y_g, x_g]

            if i < nit - 1:
                [dv, du], _, _ = feature.register_translation(g, f)
            else:
                [dv, du], _, _ = feature.register_translation(g, f, upsample_factor=10)

            u_list[j] = u + du
            v_list[j] = v + dv

        X = np.unique(np.array(grid)[:, 0])
        Y = np.unique(np.array(grid)[:, 1])
        U = np.array(u_list).reshape(len(Y), len(X))
        V = np.array(v_list).reshape(len(Y), len(X))

        # Data are smoothed to remove possible errors:
        window_size = int(2 * np.sqrt(i) + 1)
        U = ndimage.median_filter(U, size=window_size)
        V = ndimage.median_filter(V, size=window_size)

        if i < nit - 1:
            u_splines = interpolate.RegularGridInterpolator(
                (Y, X), U, "nearest", False, None
            )
            v_splines = interpolate.RegularGridInterpolator(
                (Y, X), V, "nearest", False, None
            )
        else:
            source_coords = np.array(grid) * 1.0
            target_coords = source_coords.copy()
            target_coords[:, 0] = target_coords[:, 0] + U.ravel()
            target_coords[:, 1] = target_coords[:, 1] + V.ravel()

    return source_coords, target_coords


def estimate_deformation(source_coords, target_coords):

    if len(source_coords) < 3:
        print("Not enough points.")

    affine, inliers = cv2.estimateAffine2D(source_coords, target_coords)

    if (np.sum(inliers) / len(inliers)) < 0.1:
        print("Less than a 10th of matches are used.")

    result = utilities.matrix2vector(affine)
    return result


def apply_deformation(source_coords, target_coords, grid, nrt):

    p_guess = np.empty((len(grid), 6))

    for i, (x0, y0) in enumerate(grid):
        idx = np.logical_and(
            np.logical_and(
                source_coords[:, 0] >= x0 - nrt, source_coords[:, 0] <= x0 + nrt
            ),
            np.logical_and(
                source_coords[:, 1] >= y0 - nrt, source_coords[:, 1] <= y0 + nrt
            ),
        )
        sc = source_coords[idx] - [x0, y0]
        tc = target_coords[idx] - [x0, y0]
        p = estimate_deformation(np.array(sc), np.array(tc))
        p_guess[i] = p

    return p_guess


def approximate_image(source_image, target_image, grid, guess):
    rows, cols = source_image.shape

    x_grid = np.unique(np.array(grid)[:, 0])
    y_grid = np.unique(np.array(grid)[:, 1])
    u_guess = guess[:, 0].reshape(len(y_grid), len(x_grid))
    v_guess = guess[:, 1].reshape(len(y_grid), len(x_grid))

    u_splines = interpolate.RectBivariateSpline(y_grid, x_grid, u_guess, kx=5, ky=5)
    v_splines = interpolate.RectBivariateSpline(y_grid, x_grid, v_guess, kx=5, ky=5)

    Y, X = np.mgrid[0:rows, 0:cols]
    U = u_splines.ev(Y, X)
    V = v_splines.ev(Y, X)
    x = X + U
    y = Y + V
    approx_image = ndimage.map_coordinates(target_image, (y, x), mode="nearest")
    similarity = measure.compare_ssim(source_image, approx_image, data_range=255)
    print("Approximation is {:.2%} similar.".format(similarity))

    plt.figure()
    plt.subplot(131), plt.imshow(source_image, cmap="gray")
    plt.title("Undeformed Image"), plt.axis("off")
    plt.subplot(132), plt.imshow(approx_image, cmap="gray")
    plt.title("Reconstruction"), plt.axis("off")
    plt.subplot(133), plt.imshow(target_image, cmap="gray")
    plt.title("Deformed Image"), plt.axis("off")
    plt.show()
    return approx_image


if __name__ == "__main__":
    homedir = os.path.expanduser("~")
    imgdir = os.path.join(homedir, "dic-images", "exp27")
    image1 = os.path.join(imgdir, "LP00", "000053585.tif")
    image2 = os.path.join(imgdir, "LP05", "000054007.tif")

    source = io.imread(image1).astype(np.float64)
    target = io.imread(image2).astype(np.float64)

    source = ndimage.gaussian_filter(source, 0.8)
    target = ndimage.gaussian_filter(target, 0.8)

    source_coords, target_coords = cascaded_search(source, target, 7)

    gridpt_coords = make_image_grid(source, 9, 200)
    pguess = apply_deformation(source_coords, target_coords, gridpt_coords, 15)
    approx = approximate_image(source, target, gridpt_coords, pguess)
