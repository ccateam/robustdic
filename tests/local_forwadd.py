import os
import time
import numpy as np
import engine.interpolate as interpolate
import engine.search as search
from scipy import ndimage
from skimage import measure
from main import utilities


if __name__ == "__main__":
    homedir = os.path.expanduser("~")
    imgdir = os.path.join(homedir, "dic-images", "exp13")
    source_file = os.path.join(imgdir, "LP00", "000032451.tif")
    target_file = os.path.join(imgdir, "LP16", "000034263.tif")
    source = ndimage.gaussian_filter(io.imread(source_file) * 1.0, 0.8)
    target = ndimage.gaussian_filter(io.imread(target_file) * 1.0, 0.8)

    sub = 30
    nsb = 30

    [rows, cols] = np.shape(source)
    x0 = 1150
    y0 = 752

    p0 = np.array([2.0, 83.0, 0.0, 0.0, 0.0, 0.0, 30.0])
    u0 = int(np.around(p0[0]))
    v0 = int(np.around(p0[1]))

    f = utilities.get_subset(source, sub, (x0, y0))
    target_spline = interpolate.interpolate_image(target, 5)
    spline = interpolate.interpolate_subset(
        target_spline, 5, sub + nsb, (x0 + u0, y0 + v0)
    )

    dx, dy = utilities.make_subset_grid(sub, meshed=True)

    startTime = time.time()
    res = search.fine_search(f, spline, x0, y0, dx, dy, p0)
    endTime = time.time()
    print(res.message)
    print(res.x)
    print(endTime - startTime)
