import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from utilities.arrayutils import make_image_grid


# A rounded deformation result from a test:
u, v, ux, uy, vx, vy = -3.7e00, 9.7e01, -2.8e-04, 5.9e-02, -1.0e-02, 1.6e-02
# Fabricating the data here
x0, y0 = 1225, 1026
grid = make_image_grid(None, 125, 125, 10, 10, 181, 221)
data = np.zeros((len(grid), 6))
data[..., 2:] = ux, uy, vx, vy
outs = np.random.randint(0, len(grid), 100)

for i, center in enumerate(grid):
    x, y = center
    U = u + ux * (x - x0) + vx * (y - y0)
    V = v + uy * (x - x0) + vy * (y - y0)
    data[i, 0:2] = U, V

# Adding some noise to the sata to make it more "realistic":
for i in range(6):
    noise = np.random.randn(len(grid)) * data[..., i].mean() * 0.1
    data[..., i] += noise

# Modifying some of the data by adding some exta noise to create outliers:
outs = np.random.randint(0, len(grid), 100)

for i in range(len(data)):
    if i in outs:
        data[i, ...] *= np.random.randn(6) * 1 + 1

# Scaling the data so that all parameters are evaluated equally:
scaler = StandardScaler()
pdata = scaler.fit_transform(data)

kmeans = KMeans(n_clusters=1, copy_x=True).fit(pdata)

norms = []

for i in range(len(pdata)):
    label = kmeans.labels_[i]
    clust = kmeans.cluster_centers_[label]
    norm = np.linalg.norm(pdata[i] - clust)
    norms.append(norm)

thresh = np.mean(norms) + 3 * np.std(norms)
