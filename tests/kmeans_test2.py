import pickle
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from utilities.arrayutils import make_image_grid


raw_data = pickle.load(open("analysis.p", "rb"))
data = raw_data[0, :, :]

# Scaling the data so that all parameters are evaluated equally:
scaler = StandardScaler()
pdata = scaler.fit_transform(data)

kmeans = KMeans(n_clusters=1, copy_x=True).fit(pdata)

norms = []

for i in range(len(pdata)):
    label = kmeans.labels_[i]
    clust = kmeans.cluster_centers_[label]
    norm = np.linalg.norm(pdata[i] - clust)
    norms.append(norm)

thresh = np.mean(norms) + 3 * np.std(norms)
