import os
import time
import numpy as np
import engine.interpolate as interpolate
import engine.search as search
from scipy import ndimage, optimize
from skimage import io
import utilities.imageutils as imgutil
import utilities.arrayutils as arrutil


class CostFunction:
    def __init__(self, p0, x0, y0, dx, dy, f, splines):
        self.x0 = x0
        self.y0 = y0
        self.dx = dx
        self.dy = dy
        self.f = f
        self.p0 = p0
        self.dp = np.zeros((7,))
        self.f_spline, self.g_spline = splines

        df = np.zeros((len(p0), f.shape[0], f.shape[1]))
        df[0] = self.f_spline.ev(y0 + dy, x0 + dx, dx=0, dy=1)
        df[1] = self.f_spline.ev(y0 + dy, x0 + dx, dx=1, dy=0)
        df[2] = df[0] * self.dx
        df[3] = df[1] * self.dx
        df[4] = df[0] * self.dy
        df[5] = df[1] * self.dy
        self.df = df

        self.h = 2 * np.tensordot(df, df, axes=([1, 2], [1, 2]))

        return

    def coefficient(self, p):
        p = self.p0 - p
        x = self.x0 + self.dx + p[0] + p[2] * self.dx + p[4] * self.dy
        y = self.y0 + self.dy + p[1] + p[3] * self.dx + p[5] * self.dy
        g = self.g_spline.ev(y, x, dx=0, dy=0) + p[6]
        c = np.sum((self.f - g) ** 2)
        c = c / np.sum(g ** 2)
        return c

    def jacobian(self, p):
        p = self.p0 - p
        x = self.x0 + self.dx + p[0] + p[2] * self.dx + p[4] * self.dy
        y = self.y0 + self.dy + p[1] + p[3] * self.dx + p[5] * self.dy
        g = self.g_spline.ev(y, x, dx=0, dy=0) + p[6]
        j = 2 * np.sum((self.f - g) * self.df, axis=(1, 2))
        j = j / np.sum(g ** 2)
        return j

    def hessian(self, p):
        p = self.p0 - p
        x = self.x0 + self.dx + p[0] + p[2] * self.dx + p[4] * self.dy
        y = self.y0 + self.dy + p[1] + p[3] * self.dx + p[5] * self.dy
        g = self.g_spline.ev(y, x, dx=0, dy=0) + p[6]
        h = self.h / np.sum(g ** 2)
        return h


###############################################################################
if __name__ == "__main__":
    homedir = os.path.expanduser("~")
    imgdir = os.path.join(homedir, "dic-images", "exp13")
    source_file = os.path.join(imgdir, "LP00", "000032451.tif")
    target_file = os.path.join(imgdir, "LP16", "000034263.tif")
    source = ndimage.gaussian_filter(io.imread(source_file) * 1.0, 0.8)
    target = ndimage.gaussian_filter(io.imread(target_file) * 1.0, 0.8)

    sub = 100
    nsb = 100

    [rows, cols] = np.shape(source)
    x0 = 1150
    y0 = 752

    p0 = np.array(
        [
            -1.27801297e00,
            7.98759503e01,
            -6.17516020e-02,
            6.93699672e-03,
            -1.45449890e-02,
            4.97311150e-02,
            2.06465795e01,
        ]
    )
    u0 = int(np.around(p0[0]))
    v0 = int(np.around(p0[1]))

    f = arrutil.get_subset(source, sub, (x0, y0))
    source_spline = interpolate.interpolate_image(source, 5)
    target_spline = interpolate.interpolate_image(target, 5)
    fspline = interpolate.interpolate_subset(source_spline, 5, sub + nsb, (x0, y0))
    gspline = interpolate.interpolate_subset(
        target_spline, 5, sub + nsb, (x0 + u0, y0 + v0)
    )

    dx, dy = arrutil.make_subset_grid(sub, meshed=True)

    cost = CostFunction(p0, x0, y0, dx, dy, f, (fspline, gspline))

    start_time = time.time()
    res = optimize.minimize(
        cost.coefficient,
        np.zeros((7,)),
        method="trust-ncg",
        jac=cost.jacobian,
        hess=cost.hessian,
    )
    end_time = time.time()
    print(res.message)
    print(p0 - res.x)
    print(end_time - start_time)
