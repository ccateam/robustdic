import os
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate, ndimage
from skimage import feature, io, measure, transform

import cv2
from main import utilities
from modules.basemodule import BaseOptimizer


def make_image_grid(image, spacing, pad, roi=None, show=False):
    rsize, csize = image.shape

    if roi is None:
        roi = (0, 0, csize, rsize)

    xi, yi, xf, yf = roi

    ncols = ((xf - xi) - 2 * pad + spacing) // (spacing + 1)
    nrows = ((yf - yi) - 2 * pad + spacing) // (spacing + 1)

    cpad = ((xf - xi) - 2 * pad - ncols * (spacing + 1) + spacing) // 2
    rpad = ((yf - yi) - 2 * pad - nrows * (spacing + 1) + spacing) // 2

    rows, cols = np.mgrid[0:nrows, 0:ncols] * (spacing + 1) + pad
    cols = cols + xi + cpad
    rows = rows + yi + rpad
    grid_list = np.vstack((cols.flatten(), rows.flatten()))

    if show:
        plt.imshow(image)
        plt.plot(grid_list[0, :], grid_list[1, :], "o-")
        plt.show()

    return grid_list.T.tolist()


def detect_features(image):
    """
    Detects ORB features of given image.
    PARAMETERS:
        image:
            Image to extract the descriptors from.
    RETURNS:
        Keypoints and descriptors of the image.
    """
    orb = cv2.ORB.create(nfeatures=int(1e8))
    keypoints, descriptors = orb.detectAndCompute(image.round().astype(np.uint8), None)
    return keypoints, descriptors


def match_features(source_kpts, source_desc, target_kpts, target_desc):
    """
    Matches features of two images, then estimates an affine deformation using
    the coordinates of matched features.
    PARAMETERS:
        source_kpts, source desc:
            Keypoints and descriptors of the source image.
        target_kpts, target decs:
            Keypoints and descriptors of the target image.
        draw_best:
            Threshold that determines the number of matches used in estimating
            affine deformation. Default is 100.
    RETURNS
        Estimated affine deformation between two images.
    """
    bf_matcher = cv2.BFMatcher.create(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf_matcher.match(source_desc, target_desc)
    matches = sorted(matches, key=lambda x: x.distance)

    source_coords = [source_kpts[match.queryIdx].pt for match in matches]
    target_coords = [target_kpts[match.trainIdx].pt for match in matches]

    source_coords = np.array(source_coords)
    target_coords = np.array(target_coords)

    return source_coords, target_coords


def feature_search(source, target):
    source_kpts, source_desc = detect_features(source)
    target_kpts, target_desc = detect_features(target)

    source_coords, target_coords = match_features(
        source_kpts, source_desc, target_kpts, target_desc
    )

    return source_coords, target_coords


def estimate_deformation(source_coords, target_coords):
    affine, inliers = cv2.estimateAffine2D(source_coords, target_coords)

    if (np.sum(inliers) / len(inliers)) < 0.1:
        print("Less than a 10th of matches are used.")

    result = utilities.matrix2vector(affine)
    return result


def apply_deformation(source_coords, target_coords, grid, nrt):

    p_guess = np.empty((len(grid), 6))

    for i, (x0, y0) in enumerate(grid):
        idx = np.logical_and(
            np.logical_and(
                source_coords[:, 0] >= x0 - nrt, source_coords[:, 0] <= x0 + nrt
            ),
            np.logical_and(
                source_coords[:, 1] >= y0 - nrt, source_coords[:, 1] <= y0 + nrt
            ),
        )
        sc = source_coords[idx] - [x0, y0]
        tc = target_coords[idx] - [x0, y0]
        p = estimate_deformation(np.array(sc), np.array(tc))
        p_guess[i] = p

    return p_guess


def approximate_image(source_image, target_image, grid, guess):
    rows, cols = source_image.shape

    x_grid = np.unique(np.array(grid)[:, 0])
    y_grid = np.unique(np.array(grid)[:, 1])
    u_guess = guess[:, 0].reshape(len(y_grid), len(x_grid))
    v_guess = guess[:, 1].reshape(len(y_grid), len(x_grid))

    u_splines = interpolate.RectBivariateSpline(y_grid, x_grid, u_guess, kx=5, ky=5)
    v_splines = interpolate.RectBivariateSpline(y_grid, x_grid, v_guess, kx=5, ky=5)

    Y, X = np.mgrid[0:rows, 0:cols]
    U = u_splines.ev(Y, X)
    V = v_splines.ev(Y, X)
    x = X + U
    y = Y + V
    approx_image = ndimage.map_coordinates(target, (y, x), mode="nearest")
    similarity = measure.compare_ssim(source_image, approx_image, data_range=255)
    print("Approximation is {:.2%} similar.".format(similarity))

    plt.figure()
    plt.subplot(131), plt.imshow(source_image, cmap="gray")
    plt.title("Undeformed Image"), plt.axis("off")
    plt.subplot(132), plt.imshow(approx_image, cmap="gray")
    plt.title("Reconstruction"), plt.axis("off")
    plt.subplot(133), plt.imshow(target_image, cmap="gray")
    plt.title("Deformed Image"), plt.axis("off")
    plt.show()
    return approx_image


if __name__ == "__main__":
    homedir = os.path.expanduser("~")
    imgdir = os.path.join(homedir, "dic-images", "exp27")
    image1 = os.path.join(imgdir, "LP00", "000053585.tif")
    image2 = os.path.join(imgdir, "LP05", "000054007.tif")

    source = io.imread(image1).astype(np.float64)
    target = io.imread(image2).astype(np.float64)

    source = ndimage.gaussian_filter(source, 0.8)
    target = ndimage.gaussian_filter(target, 0.8)

    source_coords, target_coords = feature_search(source, target)

    gridpt_coords = make_image_grid(source, 9, 200)
    pguess = apply_deformation(source_coords, target_coords, gridpt_coords, 50)
    approx = approximate_image(source, target, gridpt_coords, pguess)
