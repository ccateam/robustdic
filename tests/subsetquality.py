import numpy as np
from scipy import ndimage
from skimage import filters


def subset_entropy(image, sub):
    """
    Calculates subset entropy at given coordinates. Entropy values are in the
    range of 0-8 (in bits).
    PARAMETERS:
        image:
            Image to process.
        sub:
            Subset size as (2sub+1)*(2sub+1).
        coords:
            Center coordinates of subsets.
    RETURNS:
        A list of subset entropy values.
    """
    image = image.astype(np.uint8)
    width = 2 * sub + 1
    selem = np.ones((width, width))
    entropy_image = filters.rank.entropy(image, selem)

    # subset_entropy_list = [entropy_image[y, x] for x, y in coords]

    return entropy_image


def subset_sssig(image, sub):
    """
    Calculates subset SSSIG at given coordinates. Ideal SSSIG value should be
    determined empirically as it is related heavily to subset size.
    PARAMETERS:
        image:
            Image to process.
        sub:
            Subset size as (2sub+1)*(2sub+1).
        coords:
            Center coordinates of subsets.
    RETURNS:
        A list of subset SSSIG values.
    """
    width = 2 * sub + 1
    gradient_image = filters.prewitt(image) ** 2
    sssig_image = ndimage.uniform_filter(gradient_image, width) * width ** 2

    # subset_sssig_list = [sssig_image[y, x] for x, y in coords]

    return sssig_image


def ideal_subset_size(image):
    pass
