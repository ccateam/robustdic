import numpy as np
import matplotlib.pyplot as plt

from scipy import ndimage
from skimage import transform

x = np.linspace(0, 2 * np.pi, num=512, endpoint=False)

col_udf = np.sin(2 * np.pi + (x / 1.0) * 64.0) + np.sin(2 * np.pi + (x / 1.0) * 8.0)
row_udf = np.sin(2 * np.pi + (x / 1.0) * 64.0) + np.sin(2 * np.pi + (x / 1.0) * 8.0)

col_def = np.sin(2 * np.pi + (x / 1.01) * 64.0) + np.sin(2 * np.pi + (x / 1.01) * 8.0)
row_def = np.sin(2 * np.pi + (x / 0.995) * 64.0) + np.sin(2 * np.pi + (x / 0.995) * 8.0)

udfimg = (np.outer(col_udf, row_udf) / 2.0 + 1) * (255.0 / 2.0)
defimg = (np.outer(col_def, row_def) / 2.0 + 1) * (255.0 / 2.0)

udfimg_sampled = ndimage.zoom(udfimg, 0.5, order=0)
defimg_sampled = ndimage.zoom(defimg, 0.5, order=0)

affine = transform.AffineTransform(scale=(0.995, 1.01))

approx = transform.warp(defimg, affine.params, order=5, mode="edge")
approx_sampled = transform.warp(defimg_sampled, affine.params, order=5, mode="edge")
